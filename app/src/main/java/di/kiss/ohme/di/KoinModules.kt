package di.kiss.ohme.di

import android.content.Context
import android.content.SharedPreferences
import di.kiss.ohme.data.repositories.PunkRepositoryImpl
import di.kiss.ohme.data.service.PunkService
import di.kiss.ohme.domain.repositories.PunkRepository
import di.kiss.ohme.domain.useCases.GetBeerList
import di.kiss.ohme.domain.useCases.GetBeersById
import di.kiss.ohme.domain.useCases.GetSearchBeer
import di.kiss.ohme.ui.utils.SharedPreferencesConfig
import di.kiss.ohme.ui.viewmodels.PunkViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoriesModule = module {
    single { PunkService() }
    single<PunkRepository> { PunkRepositoryImpl(get()) }
}

val viewModelModule = module {
    single { PunkViewModel(get(), get(), get(), get()) }
}

val useCasesModule = module {
    single { GetBeersById(get()) }
    single { GetBeerList(get()) }
    single { GetSearchBeer(get()) }
}

val sharedPreferences = module {

    single { SharedPreferencesConfig(androidContext()) }

    single {
        getSharedPrefs(androidContext(), "com.example.android.PREFERENCE_FILE")
    }

    single<SharedPreferences.Editor> {
        getSharedPrefs(androidContext(), "com.example.android.PREFERENCE_FILE").edit()
    }
}

fun getSharedPrefs(context: Context, fileName: String): SharedPreferences {
    return context.getSharedPreferences(fileName, Context.MODE_PRIVATE)
}