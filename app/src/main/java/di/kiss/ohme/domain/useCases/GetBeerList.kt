package di.kiss.ohme.domain.useCases

import di.kiss.ohme.domain.repositories.PunkRepository
import org.koin.core.KoinComponent

class GetBeerList(private val punkRepository: PunkRepository) : KoinComponent {

    operator fun invoke(page: Int, perPage: Int) = punkRepository.getBeersList(page, perPage)
}