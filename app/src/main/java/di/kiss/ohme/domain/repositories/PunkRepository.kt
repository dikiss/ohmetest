package di.kiss.ohme.domain.repositories

import di.kiss.ohme.domain.entities.Beer
import di.kiss.ohme.domain.utils.Result

interface PunkRepository {

    fun getBeersById(id: Int)
            : di.kiss.ohme.domain.utils.Result<List<Beer>>

    fun getBeersList(page: Int, perPage: Int)
            : di.kiss.ohme.domain.utils.Result<List<Beer>>

    fun getSearchBeer(
        beerName: String,
        page: Int,
        perPage: Int
    )
            : Result<List<Beer>>
}