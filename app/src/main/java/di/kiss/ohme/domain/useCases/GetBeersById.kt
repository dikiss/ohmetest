package di.kiss.ohme.domain.useCases

import di.kiss.ohme.domain.repositories.PunkRepository
import org.koin.core.KoinComponent

class GetBeersById (private val punkRepository: PunkRepository) : KoinComponent {

    operator fun invoke(id: Int) = punkRepository.getBeersById(id)
}