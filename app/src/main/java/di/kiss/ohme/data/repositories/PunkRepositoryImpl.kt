package di.kiss.ohme.data.repositories

import di.kiss.ohme.data.service.PunkService
import di.kiss.ohme.domain.entities.Beer
import di.kiss.ohme.domain.repositories.PunkRepository
import di.kiss.ohme.domain.utils.Result

class PunkRepositoryImpl(
    private val punkService: PunkService
) : PunkRepository {

    override fun getBeersById(id: Int): Result<List<Beer>> {
        return punkService.getBeersById(id)
    }

    override fun getBeersList(page: Int, perPage: Int): Result<List<Beer>> {
        return punkService.getBeersList(page, perPage)
    }

    override fun getSearchBeer(beerName: String, page: Int, perPage: Int): Result<List<Beer>> {
        return punkService.getSearchBeer(beerName, page, perPage)
    }
}