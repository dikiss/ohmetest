package di.kiss.ohme

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import di.kiss.ohme.di.repositoriesModule
import di.kiss.ohme.di.sharedPreferences
import di.kiss.ohme.di.useCasesModule
import di.kiss.ohme.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BeerApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Fresco.initialize(this)

        startKoin {
            androidContext(this@BeerApplication)
            modules(listOf(repositoriesModule, viewModelModule, useCasesModule, sharedPreferences))
        }
    }
}